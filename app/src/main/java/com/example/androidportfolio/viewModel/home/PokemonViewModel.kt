package com.example.androidportfolio.viewModel.home

import com.example.androidportfolio.data.repository.PokemonRepository
import com.example.androidportfolio.viewModel.base.BaseViewModel

class PokemonViewModel(private val pokemonRepository: PokemonRepository) : BaseViewModel() {

    fun getListPokemon() = pokemonRepository.getListPokemon()
}