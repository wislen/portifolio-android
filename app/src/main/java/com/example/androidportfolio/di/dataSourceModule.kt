package com.example.androidportfolio.di

import com.example.androidportfolio.data.repository.DataSourceImp
import com.example.androidportfolio.data.repository.IDataSource
import com.example.androidportfolio.models.paging.PokemonDataSouce
import org.koin.dsl.module

val pokemonDataSource = module {
    single { DataSourceImp(get()) as IDataSource }
    single { PokemonDataSouce(get()) }
}