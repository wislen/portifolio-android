package com.example.androidportfolio.di

import com.example.androidportfolio.data.PokemonRepositoryImp
import com.example.androidportfolio.data.repository.PokemonRepository
import com.example.androidportfolio.models.paging.PokemonDataSouce
import com.example.androidportfolio.models.paging.PokemonDataSouceFactory
import org.koin.dsl.module

val repositoryModule = module {
    single { PokemonRepositoryImp(get()) as PokemonRepository }
    single { PokemonDataSouceFactory(get() as PokemonDataSouce) }
}