package com.example.androidportfolio.di


import com.example.androidportfolio.viewModel.home.PokemonViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { PokemonViewModel(get()) }
}