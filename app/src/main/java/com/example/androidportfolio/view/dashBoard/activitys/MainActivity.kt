package com.example.androidportfolio.view.dashBoard.activitys

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.androidportfolio.R
import com.example.androidportfolio.models.Result
import com.example.androidportfolio.view.dashBoard.adapters.PokemonAdapter
import com.example.androidportfolio.view.dashBoard.base.BaseActivity
import com.example.androidportfolio.viewModel.home.PokemonViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity() {

    private val viewModel by viewModel<PokemonViewModel>()

    private val adapter: PokemonAdapter by lazy {
        PokemonAdapter(onClickListener = { view, result ->
            callBackItemPokemonAdapter(
                view,
                result
            )
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vfDashboardContent.displayedChild = 0
        startListComponents()
        subscribeToList()
    }

    private fun startListComponents() {
        rv_pokemon.let {
            val layoutManager = GridLayoutManager(this, 3)
            it.layoutManager = layoutManager
            it.adapter = adapter
        }
    }

    private fun callBackItemPokemonAdapter(view: View, result: Result) {
        Toast.makeText(this, "Clicked: ${result.name}", Toast.LENGTH_LONG).show()
    }

    private fun subscribeToList() {
        viewModel.getListPokemon().onResult(this@MainActivity) {
            adapter.submitList(it)
            vfDashboardContent.displayedChild = 1
        }
    }
}
