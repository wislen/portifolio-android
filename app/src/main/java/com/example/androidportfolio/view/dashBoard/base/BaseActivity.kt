package com.example.androidportfolio.view.dashBoard.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe


abstract class BaseActivity : AppCompatActivity() {

    protected fun <T> LiveData<T>.onResult(owner: LifecycleOwner, action: (T) -> Unit) {
        observe(owner) { data ->
            data?.let(action)
        }
    }
}