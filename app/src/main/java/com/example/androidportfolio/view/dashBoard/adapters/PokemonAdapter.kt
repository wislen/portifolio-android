package com.example.androidportfolio.view.dashBoard.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.androidportfolio.R
import com.example.androidportfolio.models.Result
import com.example.androidportfolio.common.PokemonConst
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonAdapter(private val onClickListener: (View, Result) -> Unit) :
    PagedListAdapter<Result, PokemonAdapter.ViewHolder>(pokemonDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.itemView.setOnClickListener { view ->
                onClickListener.invoke(view, it)
            }
            holder.bind(it)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Result) {
            itemView.tv_name_pokemon.text = item.name
            val splitUrl = item.url.substringBeforeLast("/")
            val urlImage = splitUrl.substringAfterLast("/")
            val imageUrl =
                "${PokemonConst.URL_IMG_POKEMON}$urlImage${PokemonConst.FORMAT_IMG_POKEMON}"
            itemView.iv_pokemon.load(imageUrl)
        }
    }

    companion object {
        val pokemonDiff = object : DiffUtil.ItemCallback<Result>() {
            override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
                return oldItem == newItem
            }

        }
    }
}