package com.example.androidportfolio.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Activity.hideSoftKeyboard(activity: Activity) {
    val inputMethodManager = activity.getSystemService(
        Activity.INPUT_METHOD_SERVICE
    ) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
    this.currentFocus?.clearFocus()
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.showDialog(
    title: String,
    body: String,
    positiveText: String,
    positiveBlock: () -> Unit,
    negativeText: String? = null,
    negativeBlock: () -> Unit = {}
) {
    val alertDialog = AlertDialog.Builder(this).create()
    alertDialog.setTitle(title)
    alertDialog.setMessage(body)
    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveText) { _, _ ->
        positiveBlock()
    }
    negativeText?.let {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, it) { _, _ ->
            negativeBlock()
        }
    }
    alertDialog.show()
}

fun <T> Context.newIntent(classToOpen: Class<T>, bundle: Bundle? = null) {
    val intent = Intent(this, classToOpen)
    bundle?.let {
        intent.putExtras(it)
    }
    startActivity(intent)
}

inline fun <reified T> Activity.extras(key: String): Lazy<T>? = lazy {
    val value = intent.extras?.get(key)
    if (value is T) {
        value
    } else {
        throw Throwable()
    }
}
