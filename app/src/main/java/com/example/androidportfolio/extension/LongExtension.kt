package com.example.androidportfolio.extension

import java.text.SimpleDateFormat
import java.util.*

fun Long.toDate(format: String = "HH:mm"): String{
    val sdf = SimpleDateFormat(format, Locale.getDefault())
    val netDate = Date(this)
    return sdf.format(netDate)
}