package com.example.androidportfolio.data.repository

import com.example.androidportfolio.models.Pokemon
import com.example.androidportfolio.common.PokemonConst.Companion.ALL_POKEMONS
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PokemonApi {
    @GET(ALL_POKEMONS)
    fun getListPokemon(@Query("limit") limit: Int, @Query("offset") offset: Int): Deferred<Response<Pokemon>>
}