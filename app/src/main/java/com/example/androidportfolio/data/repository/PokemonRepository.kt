package com.example.androidportfolio.data.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.androidportfolio.models.Result

interface PokemonRepository {
    fun getListPokemon(): LiveData<PagedList<Result>>
}