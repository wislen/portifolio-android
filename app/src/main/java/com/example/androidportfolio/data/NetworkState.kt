package com.example.androidportfolio.data

data class NetworkState private constructor(
    val status: Status
) {

    companion object {
        val SUCCESS = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.LOADING)
        val ERROR = NetworkState(Status.FAILED)
    }
}

enum class Status(value: Int) {
    LOADING(0),
    SUCCESS(1),
    FAILED(2)
}