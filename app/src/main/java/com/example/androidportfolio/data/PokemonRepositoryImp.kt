package com.example.androidportfolio.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.androidportfolio.data.repository.PokemonRepository
import com.example.androidportfolio.models.Result
import com.example.androidportfolio.models.paging.PokemonDataSouceFactory

class PokemonRepositoryImp(private val dataSouceFactory: PokemonDataSouceFactory) :
    PokemonRepository {
    override fun getListPokemon(): LiveData<PagedList<Result>> {

        val pageSize = 20

        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 3)
            .setPrefetchDistance(10)
            .setEnablePlaceholders(false)
            .build()

        return LivePagedListBuilder(dataSouceFactory, config).build()
    }

}