package com.example.androidportfolio.data.repository

import androidx.paging.PageKeyedDataSource
import com.example.androidportfolio.models.Result

interface IDataSource {

    fun createObservable(
        requestPage: Int,
        nextPage: Int,
        requestLoadSize: Int,
        loadInitialCallback: PageKeyedDataSource.LoadInitialCallback<Int, Result>?,
        loadCallback: PageKeyedDataSource.LoadCallback<Int, Result>?
    )
}