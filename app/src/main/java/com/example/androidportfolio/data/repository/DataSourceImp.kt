package com.example.androidportfolio.data.repository

import androidx.paging.PageKeyedDataSource
import com.example.androidportfolio.models.Result
import com.example.androidportfolio.viewModel.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DataSourceImp(
    private val api: PokemonApi
) : BaseViewModel(), IDataSource {
    override fun createObservable(
        requestPage: Int,
        nextPage: Int,
        requestLoadSize: Int,
        loadInitialCallback: PageKeyedDataSource.LoadInitialCallback<Int, Result>?,
        loadCallback: PageKeyedDataSource.LoadCallback<Int, Result>?
    ) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                api.getListPokemon(requestLoadSize, requestPage * requestLoadSize).await()
            }.let {
                withContext(Dispatchers.Main) {
                    it.body()?.results?.let { pokemonList ->
                        loadInitialCallback?.onResult(
                            pokemonList,
                            null,
                            nextPage
                        )
                        loadCallback?.onResult(
                            pokemonList,
                            nextPage
                        )
                    }
                }
            }
        }
    }
}