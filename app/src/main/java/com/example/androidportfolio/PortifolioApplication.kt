package com.example.androidportfolio

import android.app.Application
import com.example.androidportfolio.di.pokemonDataSource
import com.example.androidportfolio.di.repositoryModule
import com.example.androidportfolio.di.retrofitModule
import com.example.androidportfolio.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PortifolioApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PortifolioApplication)
            modules(
                listOf(
                    retrofitModule,
                    repositoryModule,
                    viewModelModule,
                    pokemonDataSource
                )
            )
        }
    }
}