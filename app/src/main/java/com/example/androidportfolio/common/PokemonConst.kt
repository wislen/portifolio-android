package com.example.androidportfolio.common

interface PokemonConst {
    companion object {
        const val BASE_URL = "https://pokeapi.co/api/v2/"
        const val ALL_POKEMONS = "pokemon/"
        const val URL_IMG_POKEMON =
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
        const val FORMAT_IMG_POKEMON = ".png"
        const val LOG = "POKEMON"
        const val ERROR = "ERROR:"
    }
}