package com.example.androidportfolio.models

data class Result(
    val name: String,
    val url: String
)