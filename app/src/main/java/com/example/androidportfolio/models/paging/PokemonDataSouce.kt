package com.example.androidportfolio.models.paging

import androidx.paging.PageKeyedDataSource
import com.example.androidportfolio.data.repository.IDataSource
import com.example.androidportfolio.models.Result

class PokemonDataSouce(
    private val iDataSource: IDataSource
) : PageKeyedDataSource<Int, Result>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Result>
    ) {
        iDataSource.createObservable(0, 1, params.requestedLoadSize, callback, null)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        val page = params.key
        iDataSource.createObservable(page, page.inc(), params.requestedLoadSize, null, callback)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        val page = params.key
        iDataSource.createObservable(page, page.dec(), params.requestedLoadSize, null, callback)
    }
}