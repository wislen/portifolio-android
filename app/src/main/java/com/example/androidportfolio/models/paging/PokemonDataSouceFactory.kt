package com.example.androidportfolio.models.paging

import androidx.paging.DataSource
import com.example.androidportfolio.models.Result

class PokemonDataSouceFactory(
    private val pokemonDataSource: PokemonDataSouce
) : DataSource.Factory<Int, Result>() {
    override fun create(): DataSource<Int, Result> {
        return pokemonDataSource
    }
}